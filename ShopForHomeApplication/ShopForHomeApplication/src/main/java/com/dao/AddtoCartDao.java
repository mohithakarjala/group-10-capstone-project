package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bean.AddtoCart;



public interface AddtoCartDao {

	public interface AdminDao extends JpaRepository<AddtoCart, String> {
}
}