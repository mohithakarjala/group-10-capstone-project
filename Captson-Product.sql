create database productshop;
use productshop;

create table admin(
email varchar(20) primary key,
password varchar(20));

create table users(
email varchar(20)  primary key ,
password varchar(20));

create table product(
pid int primary key,
pname varchar(40),
price float,
url varchar(10000),
category varchar(50));

create table addtocart(
email varchar(20) primary key,
password varchar(20));
pid int primary key,
pname varchar(40),
price float,
url varchar(10000),
category varchar(50));